from django.conf import settings


COUCHDB_DATASTORE_HOST = getattr(settings, 'COUCHDB_DATASTORE_HOST', 'http://127.0.0.1:5984')
COUCHDB_DATASTORE_USER = getattr(settings, 'COUCHDB_DATASTORE_USER', 'admin')
COUCHDB_DATASTORE_PASSWORD = getattr(settings, 'COUCHDB_DATASTORE_PASSWORD', 'couchdb')
# couchdb db index
COUCHDB_DATASTORE_DATABASE_NAME = getattr(settings, 'COUCHDB_DATASTORE_DATABASE_NAME', 'django_couchdb_datastore')

_COUCHDB_DATASTORE_EXCLUDED_MODELS = [
    # 'migrations',
    'auth.permission',
    'contenttypes',
]
COUCHDB_DATASTORE_EXCLUDED_MODELS = _COUCHDB_DATASTORE_EXCLUDED_MODELS + [
    'sessions',
    'admin'
]
