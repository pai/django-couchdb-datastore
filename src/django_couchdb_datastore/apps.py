from django.apps import AppConfig


class DjangoCouchdbDatastoreConfig(AppConfig):
    name = 'django_couchdb_datastore'
    verbose_name = 'Django Couchdb Datastore'

    def ready(self):
        try:
            from . import signals  # noqa F401

        except ImportError:
            pass
