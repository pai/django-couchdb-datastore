from cloudant.client import CouchDB
from cloudant.database import CloudantDatabaseException

from .settings import (COUCHDB_DATASTORE_HOST, COUCHDB_DATASTORE_USER, COUCHDB_DATASTORE_PASSWORD,
                       COUCHDB_DATASTORE_DATABASE_NAME)


class DBAdapter:
    """
    CouchDB adapter
    """
    def __init__(self):
        self.client = CouchDB(COUCHDB_DATASTORE_USER, COUCHDB_DATASTORE_PASSWORD,
                              url=COUCHDB_DATASTORE_HOST,
                              connect=True,
                              auto_renew=True,
                              use_basic_auth=True)

        try:
            self.database = self.client[COUCHDB_DATASTORE_DATABASE_NAME]
        except KeyError:
            self.database = self.client.create_database(COUCHDB_DATASTORE_DATABASE_NAME)

    def __del__(self):
        self.client.disconnect()

    def bulk_docs(self, docs):
        return self.database.bulk_docs(docs)

    def put_document(self, obj):
        try:
            res = self.database.create_document(obj, throw_on_exists=True)
            return True

        except CloudantDatabaseException:
            updated = False

            while not updated:
                existing = self.database.all_docs(key=obj['_id'])['rows'][0]
                # would return {'id': '1', 'key': '1', 'value': {'rev': 'n-xxxxxxx'}}

                existing_rev = existing['value']['rev']
                obj['_rev'] = existing_rev

                _updated = self.bulk_docs([obj])
                # would return [{'ok': True, 'id': '1', 'rev': 'n-xxxxxxx'}]
                updated = len(list(filter(lambda x: x['ok'], filter(lambda x: x['id'] == obj['_id'], _updated)))) > 0

            return updated  # True

        else:
            return True

    def get_documents(self):
        return self.database.all_docs(include_docs=True)
